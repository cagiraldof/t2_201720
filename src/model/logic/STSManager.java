package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.ObjectInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOStopTimes;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.Node;
import model.data_structures.RingList;

public class STSManager implements ISTSManager {

	@Override
	public void loadRoutes(String routesFile) {
		// TODO Auto-generated method stub
		importarDatosCadena(routesFile);

	}

	@Override
	public void loadTrips(String tripsFile) {
		// TODO Auto-generated method stub
		importarDatosCadena(tripsFile);

	}

	@Override
	public void loadStopTimes(String stopTimesFile) {
		// TODO Auto-generated method stub

	}

	@Override
	public void loadStops(String stopsFile) {
		// TODO Auto-generated method stub

	}

	@Override
	public IList<VORoute> routeAtStop(String stopName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOStop> stopsRoute(String routeName, String direction) {
		// TODO Auto-generated method stub
		return null;
	}

	public DoubleLinkedList importarDatosCadena( String tipoArchivo ) 
	{
		DoubleLinkedList cadenaDoble = new DoubleLinkedList();
		File pArchivo = new File(tipoArchivo);
		try{
			BufferedReader bf = new BufferedReader( new FileReader( pArchivo ) );
			String linea = bf.readLine( );
			while( linea != null )
			{
				if (tipoArchivo.equals("./data/routes.txt"))
				{
					String[] partesDato = linea.split( "," );
					Node nodo =new Node<>(new VORoute(partesDato[0], partesDato[1], partesDato[2], partesDato[3], partesDato[4], partesDato[5], partesDato[6], partesDato[7], partesDato[8]));
					cadenaDoble.agregarAlFinal(nodo);
				}
				else if(tipoArchivo.equals("./data/stop_times.txt"))
				{
					String[] partesDato = linea.split( "," );
					Node nodo =new Node<>(new VOStopTimes(partesDato[0], partesDato[1], partesDato[2], partesDato[3], partesDato[4], partesDato[5], partesDato[6], partesDato[7], partesDato[8]));
					cadenaDoble.agregarAlFinal(nodo);
				}

				linea = bf.readLine( );
			}
			bf.close( );
			return cadenaDoble;
		}
		catch(Exception e){
			return cadenaDoble;
		}
	}
	
	public RingList importarDatosCircular( String tipoArchivo ) 
	{
		RingList listaCircular= new RingList();
		File pArchivo = new File(tipoArchivo);
		try{
			BufferedReader bf = new BufferedReader( new FileReader( pArchivo ) );
			String linea = bf.readLine( );
			while( linea != null )
			{
				if (tipoArchivo.equals("./data/stops.txt"))
				{
					String[] partesDato = linea.split( "," );
					Node nodo =new Node<>(new VOStop(partesDato[0], partesDato[1], partesDato[2], partesDato[3], partesDato[4], partesDato[5], partesDato[6], partesDato[7], partesDato[8]));
					listaCircular.agregarAlFinal(nodo);
				}
				else if(tipoArchivo.equals("./data/trips.txt"))
				{
					String[] partesDato = linea.split( "," );
					Node nodo =new Node<>(new VOTrips(partesDato[0], partesDato[1], partesDato[2], partesDato[3], partesDato[4], partesDato[5], partesDato[6], partesDato[7], partesDato[8]));
					listaCircular.agregarAlFinal(nodo);
				}

				linea = bf.readLine( );
			}
			bf.close( );
			return cadenaDoble;
		}
		catch(Exception e){
			return cadenaDoble;
		}
	}

}
