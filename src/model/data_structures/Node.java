package model.data_structures;

public class Node<E>
{
	private Node<E> siguiente;
	private Node<E> anterior;
	private E object;

	public Node(E obj)
	{
		object =  obj;
		anterior = null;
		siguiente = null;
	}

	public void setNext(Node<E> next)
	{
		siguiente=next;
	}

	public void setPrev(Node<E> prev)
	{
		anterior=prev;
	}	
	public void setObj(E obj) {
		object =obj;
	}
	public Node<E> getNext()
	{
		return siguiente;
	}
	public Node<E> getPrev()
	{
		return anterior;
	}
	public E getObj()
	{
		return object;
	}
}
