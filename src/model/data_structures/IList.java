 package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> {

public void agregarAlFinal(T obj) ;
public T darObj(int pos);
public T darObjetoActual();
public int darTamanho();
public boolean siguiente();
public boolean anterior();


	


}
