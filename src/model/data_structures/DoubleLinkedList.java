package model.data_structures;

import java.util.Iterator;

public class DoubleLinkedList <T> implements IList <T>
{
	private Node<T> primer;

	private Node<T>ultimo;

	private int tamanho;

	Iterator<T>iterador=this.iterator();

	public DoubleLinkedList()
	{
		primer = null;
		ultimo = null;
		tamanho=0;
	}

	public boolean agregarEnPos(int posicion, Node<T> nodoAAgregar)
	{
		Node<T> nodoEnPos=darNodoPos(posicion);
		boolean sePudo=false;
		if(nodoEnPos!=null) 
		{   if(posicion==0)
			primer=nodoAAgregar;
		if(posicion==tamanho-1){
			{
				ultimo=nodoAAgregar;
			}
		}
		if(posicion>0) 
		{
			nodoEnPos.getPrev().setNext(nodoAAgregar);
			nodoAAgregar.setNext(nodoEnPos);
		}
		nodoEnPos.setPrev(nodoAAgregar);
		nodoAAgregar.setNext(nodoEnPos);

		sePudo=true;
		tamanho++;
		}
		else if(posicion==0) {
			primer=nodoAAgregar;
			ultimo=nodoAAgregar;
			sePudo=true;
		}
		return sePudo;
	}
	public Node<T> darNodoPos(int posicion)
	{
		if(posicion>=tamanho)
			return null;
		Node<T> actual=primer;
		int pos=0;
		if(actual!=null) 
		{
			while(pos<posicion)
			{
				actual=actual.getNext();
				pos++;
			}

		}
		return actual;
	}

	public boolean eliminarEnPos(int posicion)
	{
		Node<T> nodoEnPos=darNodoPos(posicion);
		boolean sePudo=false;
		if(nodoEnPos!=null) 
		{
			if(posicion>0&&posicion<tamanho-1) 
			{
				nodoEnPos.getPrev().setNext(nodoEnPos.getNext());
				nodoEnPos.getNext().setPrev(nodoEnPos.getPrev());
				sePudo=true;
				tamanho--;
			}
			else if(posicion==0) 
			{   primer=nodoEnPos.getNext();
			nodoEnPos.getNext().setPrev(null);
			sePudo=true;
			tamanho--;
			}
			else if(posicion==tamanho-1) 
			{	ultimo=nodoEnPos.getPrev();
			nodoEnPos.getPrev().setNext(null);
			sePudo=true;
			tamanho--;
			}
		}
		return sePudo;
	}

	public int darTamanho()
	{
		return tamanho;
	}



	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub		
		return new Iterator<T>() {
			private int pos = -1;
			@Override
			public boolean hasNext() {
				if(tamanho == 0)
					return false;

				return pos < ( tamanho - 1 );
			}

			@Override
			public T next() {
				return darObj(++pos);
			}	
		};
	}

	@Override
	public void agregarAlFinal(T obj) {
		// TODO Auto-generated method stub
		Node <T> nuevo=new Node<T>(obj);
		if(ultimo==null) 
		{
			primer=nuevo;
			ultimo=nuevo;
		}
		else 
		{
			ultimo.setNext(nuevo);
			nuevo.setPrev(ultimo);
			ultimo=nuevo;
		}

	}

	@Override
	public T darObj(int pos) {
		// TODO Auto-generated method stub
		Node<T> encontrado=darNodoPos(pos);
		T obj=null;
		if(encontrado!=null)
			obj=encontrado.getObj();
		return obj;
	}

	@Override
	public T darObjetoActual() {
		// TODO Auto-generated method stub
		return null;
	}

	

	@Override
	public boolean siguiente() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean anterior() {
		// TODO Auto-generated method stub
		return false;
	}
}
