package model.data_structures;

import java.util.Iterator;

public class RingList <T> implements IList <T>{

	private Node<T> primer;
	private int tamanho;

	public RingList()
	{
		primer = null;
		tamanho = 0;
	}

	public boolean agregarEnPos(int posicion, Node<T> nodoAAgregar)
	{
		boolean sePudo=false;

		//compruebo esto antes de operar % en darNodoPos() para no dividir entre 0
		if(posicion==0&&tamanho==0) {
			primer=nodoAAgregar;
			nodoAAgregar.setNext(nodoAAgregar);
			nodoAAgregar.setPrev(nodoAAgregar);
			sePudo=true;
		}
		else {
			Node<T> nodoEnPos=darNodoPos(posicion);

			if(nodoEnPos!=null) 
			{   if(posicion==0)
				primer=nodoAAgregar;

			if(posicion>0) 
			{
				nodoEnPos.getPrev().setNext(nodoAAgregar);
				nodoAAgregar.setNext(nodoEnPos);
			}
			nodoEnPos.setPrev(nodoAAgregar);
			nodoAAgregar.setNext(nodoEnPos);

			sePudo=true;
			tamanho++;
			}
			else if(posicion==0) {
				primer=nodoAAgregar;
				sePudo=true;
			}
		}
		return sePudo;
	}

	public Node<T> darNodoPos(int position)
	{  	
		Node<T> actual=primer;
		if(actual!=null)
		{
			int posicion=position%tamanho;

			int pos=0;
			if(actual!=null) 
			{
				while(pos<posicion)
				{
					actual=actual.getNext();
					pos++;
				}

			}
		}
		return actual;
	}


	public boolean eliminarEnPos(int posicion)
	{
		Node<T> nodoEnPos=darNodoPos(posicion);
		boolean sePudo=false;
		if(nodoEnPos!=null) 
		{
			nodoEnPos.getPrev().setNext(nodoEnPos.getNext());
			nodoEnPos.getNext().setPrev(nodoEnPos.getPrev());
			if(nodoEnPos==primer) {
				primer=nodoEnPos.getNext();
			}
			sePudo=true;


		}
		return sePudo;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub		
		return new Iterator<T>() {
			private int pos = -1;
			@Override
			public boolean hasNext() {
				if(tamanho == 0)
					return false;
				else {
					return true;
				}
			}


			@Override
			public T next() {
				return darObj(++pos%tamanho);
			}	
		};
	}

	@Override
	public void agregarAlFinal(T obj) {
		eliminarEnPos(tamanho-1);

	}

	@Override
	public T darObj(int pos) {
		Node<T> encontrado=darNodoPos(pos);
		T dato=null;
		if(encontrado!=null) {
			dato=encontrado.getObj();
		}
		return dato;
	}

	@Override
	public T darObjetoActual() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override

	public boolean siguiente() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean anterior() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int darTamanho() {
		return tamanho;
	}

}
