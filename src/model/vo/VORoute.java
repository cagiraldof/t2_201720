package model.vo;

/**
 * Representation of a route object
 */
public class VORoute {

	private int routeId;
	private String routeAgency;
	private String routeSh_Name;
	private String routeLon_Name;
	private String routeDescript;
	private int routeType;
	private String routeURL;
	private String routeColor;
	private String routeTxtColor;
	
	public VORoute(String id, String agency, String sh_Name, String lo_Name, String descr, String type,String url, String col, String txt_Col)
	{
		routeId = Integer.parseInt(id);
		routeAgency = agency;
		routeColor = col;
		routeDescript = descr;
		routeLon_Name= lo_Name;
		routeSh_Name = sh_Name;
		routeTxtColor= txt_Col;
		routeType = Integer.parseInt(type);
		routeURL = url;
	}
	/**
	 * @return id - Route's id number
	 */
	public int routeId() {
		// TODO Auto-generated method stub
		return routeId;
	}
	
	public String agency () {
		// TODO Auto-generated method stub
		return routeAgency;
	}
	
	public String color() {
		// TODO Auto-generated method stub
		return routeColor;
	}
	
	public String description() {
		// TODO Auto-generated method stub
		return routeDescript;
	}
	
	public String longName() {
		// TODO Auto-generated method stub
		return routeLon_Name;
	}
	
	public String shortName() {
		// TODO Auto-generated method stub
		return routeSh_Name;
	}
	
	public String txtColor() {
		// TODO Auto-generated method stub
		return routeTxtColor;
	}
	
	public String getURL() {
		// TODO Auto-generated method stub
		return routeURL;
	}
	
	public int Type() {
		// TODO Auto-generated method stub
		return routeType;
	}
	
	/**
	 * @return id - stop's id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return name - stop name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

}
