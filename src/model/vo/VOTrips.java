package model.vo;

import com.sun.javafx.iio.common.RoughScaler;

public class VOTrips {

	private int routeId;
	private int routesService;
	private String routeTrip;
	private String tripHeadsign;
	private String tripShortName;
	private int routeDirection;
	private int routeBlock;
	private int routeShape;
	private Boolean wheelchairAccess;
	private Boolean bikesAllowed;
	
	public VOTrips(String id, String service, String route, String headsign, String shortN, String direction, String block,String shape,String wheelchair,String bikes) {
		routeId=Integer.parseInt(id);
		routesService=Integer.parseInt(service);
		routeTrip=route;
		tripHeadsign=headsign;
		tripShortName=shortN;
		routeDirection=Integer.parseInt(direction);
		routeBlock=Integer.parseInt(block);
		routeShape=Integer.parseInt(shape);
		wheelchairAccess=Boolean.parseBoolean(wheelchair);
		bikesAllowed=Boolean.parseBoolean(bikes);
		
	}
}
