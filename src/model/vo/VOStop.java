package model.vo;

/**
 * Representation of a Stop object
 */
public class VOStop {

	private int stopId;
	private int code;
	private String name;
	private String desc;
	private String lat;
	private String lon; 
	private String zoneId;
	private String stopURL;
	private int type;
	private String station;

	public  VOStop(String pStopId,String pCode,String pName,String pDesc,String pLat,String pLon,String pZoneId,String pStopURL,String pType, String pStation)
	{
		int stopId = Integer.parseInt(pStopId);
		int code = Integer.parseInt(pCode);
		String name= pName;
		String desc= pDesc;
		String lat= pLat;
		String lon= pLon; 
		String zoneId= pZoneId;
		String stopURL= pStopURL;
		int type = Integer.parseInt(pType);
		String station= pStation;
	}
	/**
	 * @return id - Route's id number
	 */
	
	/**
	 * @return id - stop's id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return name - stop name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
}